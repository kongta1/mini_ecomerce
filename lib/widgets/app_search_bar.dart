import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as badges;

class AppSearchBar extends StatelessWidget {
  final int cartItemCount; // Number of items in the cart
  final VoidCallback onMenuTap; // Callback to handle menu icon tap

  AppSearchBar({this.cartItemCount = 0, required this.onMenuTap});

  badges.BadgePosition calculateBadgePosition(String content) {
    // Calculate offsets based on content length
    int length = content.length;
    double topOffset = -10;
    double endOffset = -10;

    if (length > 1) {
      endOffset -= length * 3; // Adjust end position based on content length
    }

    return badges.BadgePosition.topEnd(top: topOffset, end: endOffset);
  }

  @override
  Widget build(BuildContext context) {
    String cartCountString = cartItemCount.toString();
    return Padding(
      padding: const EdgeInsets.fromLTRB(
          16, 20, 16, 0), // Adjust padding as necessary
      child: Row(
        children: [
          IconButton(
            icon: const Icon(Icons.menu,
                color: Colors.black), // Drawer toggle icon
            onPressed: onMenuTap, // Use the callback to open/close the drawer
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(
                  left:
                      8), // Provide some space between menu icon and search bar
              padding: const EdgeInsets.symmetric(horizontal: 8),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
              ),
              child: const TextField(
                decoration: InputDecoration(
                  hintText: "Search Product",
                  border: InputBorder.none,
                  prefixIcon: Icon(Icons.search, color: Colors.grey),
                ),
              ),
            ),
          ),
          IconButton(
            icon: badges.Badge(
              badgeContent: Text(
                cartItemCount.toString(),
                style: const TextStyle(color: Colors.white),
              ),
              position: calculateBadgePosition(cartCountString),
              child: const Icon(Icons.shopping_cart, color: Colors.black),
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/cart');
            },
          ),
          IconButton(
            icon: const Icon(Icons.notifications_none, color: Colors.black),
            onPressed: () {
              // Handle notifications press
            },
          ),
        ],
      ),
    );
  }
}
