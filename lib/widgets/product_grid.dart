
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mini_ecommerce/components/item_card.dart';
import 'package:mini_ecommerce/constants.dart';
import 'package:mini_ecommerce/models/Product.dart';
import 'package:mini_ecommerce/widgets/detail_screen.dart';

class ProductGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: kDefaultPaddin),
        child: StreamBuilder<QuerySnapshot>(
            stream:
                FirebaseFirestore.instance.collection('product_1').snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return const Text('Something went wrong');
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }

              var products =
                  snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                return Product(
                  id: document.id,
                  title: data['title'],
                  description: data['description'],
                  price: data['price'],
                  size: data['size'],
                  image: data['image'],
                  color: Color(int.parse(data['color'])),
                );
              }).toList();

              return GridView.builder(
                physics: const BouncingScrollPhysics(),
                itemCount: products.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: kDefaultPaddin,
                  crossAxisSpacing: kDefaultPaddin,
                  childAspectRatio: 0.75,
                ),
                itemBuilder: (context, index) {
                  // Get the current product
                  Product product = products[index];

                  return ItemCard(
                    product: product,
                    title: product.title,
                    image: product.image,
                    price: product.price,
                    size: product.size,
                    color: product.color,
                    press: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailsScreen(product: product),
                      ),
                    ),
                  );
                },
              );
            }),
      ),
    );
  }
}
