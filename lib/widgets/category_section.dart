import 'package:flutter/material.dart';

class Category {
  final String name;
  final IconData icon;

  Category({required this.name, required this.icon});
}

final List<Category> categories = [
  Category(name: "Pizza", icon: Icons.local_pizza),
  Category(name: "Hamburger", icon: Icons.fastfood),
  Category(name: "Spaghetti", icon: Icons.ramen_dining),
  Category(name: "Salads", icon: Icons.eco),
  Category(name: "Drinks", icon: Icons.local_cafe),
  Category(name: "Desserts", icon: Icons.cake),
  Category(name: "Seafood", icon: Icons.set_meal),
  Category(name: "Steak", icon: Icons.restaurant),
  Category(name: "Breakfast", icon: Icons.free_breakfast),
  Category(name: "Ice Cream", icon: Icons.icecream),
];

class CategorySection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100, // Adjust height to fit the category icon and text properly
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (context, index) {
          final category = categories[index];
          return GestureDetector(
            onTap: () {
              // Implement action on tap if needed, e.g., navigate to category-specific page
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 30, // Icon size can be adjusted
                    backgroundColor: Colors.purple[100],
                    child: Icon(category.icon, color: Colors.purple, size: 24),
                  ),
                  const SizedBox(height: 5),
                  Text(category.name,
                      style: const TextStyle(
                          fontSize: 12)), // Adjust text size as needed
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
