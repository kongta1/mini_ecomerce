import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mini_ecommerce/providers/locale_provider.dart';
import 'package:mini_ecommerce/widgets/manageProduct.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart'; // Assuming localization setup

class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  User? user;

  @override
  void initState() {
    super.initState();
    user = FirebaseAuth.instance.currentUser;
  }

  String extractUsername(String email) {
    int atIndex = email.indexOf('@');
    return atIndex != -1 ? email.substring(0, atIndex) : "Invalid email";
  }

  void _changeLanguage(String languageCode) {
    // Assume a LocaleProvider exists and is accessible via context
    var localeProvider = Provider.of<LocaleProvider>(context, listen: false);
    localeProvider.setLocale(Locale(languageCode));
  }

  @override
  Widget build(BuildContext context) {
    final localization = AppLocalizations.of(context);

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Colors.purple,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                CircleAvatar(
                  radius: 30,
                  backgroundImage: user?.photoURL != null
                      ? NetworkImage(user!.photoURL!)
                      : const AssetImage('assets/images/profile/Kenshin.jpg'),
                  backgroundColor: Colors.transparent,
                ),
                const SizedBox(height: 10),
                Text(
                  extractUsername(user?.email ?? 'No Name Available'),
                  style: const TextStyle(color: Colors.white, fontSize: 20),
                ),
                Text(
                  user?.email ?? 'No Email Available',
                  style: const TextStyle(color: Colors.white, fontSize: 14),
                ),
              ],
            ),
          ),
          ListTile(
            leading: const Icon(Icons.store),
            title: Text(localization!.managementStore),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ManageProductPage()));
            },
          ),
          ListTile(
            leading: const Icon(Icons.language),
            title: Text(localization!.language),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text(localization!.selectLanguage),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ListTile(
                          title: const Text('English'),
                          onTap: () {
                            _changeLanguage('en');
                            Navigator.pop(context);
                          },
                        ),
                        ListTile(
                          title: const Text('ລາວ'), // Lao
                          onTap: () {
                            _changeLanguage('lo');
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  );
                },
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: Text(localization!.settings),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/settings');
            },
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: Text(localization!.logout),
            onTap: () {
              Navigator.pushNamed(context, '/login');
            },
          ),
        ],
      ),
    );
  }
}
