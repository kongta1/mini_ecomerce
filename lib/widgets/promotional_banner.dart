import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class Promotion {
  final String imageUrl;
  final String description;

  Promotion({required this.imageUrl, required this.description});
}

class PromotionalBanner extends StatefulWidget {
  @override
  _PromotionalBannerState createState() => _PromotionalBannerState();
}

class _PromotionalBannerState extends State<PromotionalBanner> {
  int _current = 0;
  final List<Promotion> promotions = [
    Promotion(
      imageUrl: 'assets/images/promotion/promotion1.jpg',
      description: "Weekend Pizza Party! 50% off all pizzas. Don’t miss out!",
    ),
    Promotion(
      imageUrl: 'assets/images/promotion/promotion2.jpg',
      description: "Juicy Hamburger Special! Only 399. Grab yours today!",
    ),
    Promotion(
      imageUrl: 'assets/images/promotion/promotion3.jpg',
      description: "Taste Italy with our Homemade Spaghetti for just \$15!",
    ),
  ];
  final CarouselController _carouselController = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 10),
        CarouselSlider(
          carouselController: _carouselController,
          items: promotions
              .map((promo) => ClipRRect(
                    borderRadius:
                        BorderRadius.circular(15.0), // Rounded corners
                    child: Stack(
                      fit: StackFit.expand,
                      children: [
                        Image.asset(
                          promo.imageUrl,
                          fit: BoxFit.fill,
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Colors.transparent,
                                  Colors.black.withOpacity(0.7)
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                              ),
                            ),
                            padding: const EdgeInsets.all(20),
                            child: Text(
                              promo.description,
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ))
              .toList(),
          options: CarouselOptions(
            autoPlay: true,
            enlargeCenterPage: true,
            aspectRatio: 2.0,
            onPageChanged: (index, reason) {
              setState(() {
                _current = index;
              });
            },
            autoPlayCurve: Curves.fastOutSlowIn,
            autoPlayInterval: Duration(seconds: 3),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: promotions.asMap().entries.map((entry) {
            return GestureDetector(
              onTap: () => _carouselController.animateToPage(entry.key),
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 500),
                width: _current == entry.key ? 12.0 : 8.0,
                height: _current == entry.key ? 12.0 : 8.0,
                margin:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == entry.key
                      ? Colors.red
                      : Colors.grey.withOpacity(0.4),
                ),
              ),
            );
          }).toList(),
        ),
      ],
    );
  }
}
