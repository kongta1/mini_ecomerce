import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:math';

class ManageProductPage extends StatefulWidget {
  @override
  _ManageProductPageState createState() => _ManageProductPageState();
}

class _ManageProductPageState extends State<ManageProductPage>
    with TickerProviderStateMixin {
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final imageController = TextEditingController();
  final priceController = TextEditingController();
  final sizeController = TextEditingController();
  Color currentColor = const Color(0xff443a49);

  Color getRandomColor() {
    Random random = Random();
    int r = random.nextInt(256); // Red
    int g = random.nextInt(256); // Green
    int b = random.nextInt(256); // Blue
    // Combining color values with full opacity
    return Color.fromRGBO(r, g, b, 1);
  }

  String colorToString(Color color) {
    // Return the color as a string in the format '0xFFRRGGBB'
    return '0xFF${color.red.toRadixString(16).padLeft(2, '0')}${color.green.toRadixString(16).padLeft(2, '0')}${color.blue.toRadixString(16).padLeft(2, '0')}';
  }

  @override
  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    priceController.dispose();
    sizeController.dispose();
    imageController.dispose();
    super.dispose();
  }

  void addProduct() async {
    if (titleController.text.isNotEmpty && priceController.text.isNotEmpty) {
      try {
        Color newColor = getRandomColor();
        String colorString = colorToString(newColor);

        await FirebaseFirestore.instance.collection('products').add({
          'color': colorString, // Store color as a hex string
          'description': descriptionController.text,
          'image': imageController.text,
          'price': int.parse(priceController.text),
          'size': int.parse(sizeController.text),
          'title': titleController.text,
        });

        // Clear the text fields
        titleController.clear();
        descriptionController.clear();
        imageController.clear();
        priceController.clear();
        sizeController.clear();

        Navigator.of(context).pop();
      } catch (e) {
        print('Error adding product: $e');
        showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: const Text('Error'),
            content: Text('Failed to add product. Error: $e'),
            actions: <Widget>[
              TextButton(
                child: const Text('Ok'),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              ),
            ],
          ),
        );
      }
    } else {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: const Text('Error'),
          content: const Text('Please fill form correctly'),
          actions: <Widget>[
            TextButton(
              child: const Text('Ok'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            ),
          ],
        ),
      );
    }
  }

  void _showEditProductDialog(DocumentSnapshot document) {
    Map<String, dynamic> data = document.data() as Map<String, dynamic>;

    // Set the controllers without clearing them first
    titleController.text = data['title'];
    descriptionController.text = data['description'];
    imageController.text = data['image'];
    priceController.text = data['price'].toString();
    sizeController.text = data['size'].toString();

    showDialog(
      context: context,
      barrierDismissible: false, // Ensure dialog must be closed intentionally
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Edit Product"),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  controller: titleController,
                  autofocus: true, // Focus on the first field when dialog opens
                  decoration: const InputDecoration(labelText: 'Title'),
                ),
                TextField(
                  controller: descriptionController,
                  decoration: const InputDecoration(labelText: 'Description'),
                ),
                TextField(
                  controller: imageController,
                  decoration: const InputDecoration(labelText: 'Path Image'),
                ),
                TextField(
                  controller: priceController,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(labelText: 'Price'),
                ),
                TextField(
                  controller: sizeController,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(labelText: 'Size'),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text("Cancel"),
              onPressed: () {
                // Clear controllers when canceling
                titleController.clear();
                descriptionController.clear();
                imageController.clear();
                priceController.clear();
                sizeController.clear();
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              onPressed: () {
                updateProduct(document.id);
                Navigator.of(context).pop();
              },
              child: const Text("Update"),
            ),
          ],
        );
      },
    );
  }

  void updateProduct(String docId) async {
    try {
      await FirebaseFirestore.instance
          .collection('products')
          .doc(docId)
          .update({
        'title': titleController.text,
        'description': descriptionController.text,
        'image': imageController.text,
        'price': int.parse(priceController.text),
        'size': int.parse(sizeController.text),
      });
      // Clear the text fields after updating
      titleController.clear();
      descriptionController.clear();
      imageController.clear();
      priceController.clear();
      sizeController.clear();
    } catch (e) {
      print('Error updating product: $e');
    }
  }

  void _showAddProductDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Add a New Product"),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  controller: titleController,
                  decoration: const InputDecoration(labelText: 'Title'),
                ),
                TextField(
                  controller: descriptionController,
                  decoration: const InputDecoration(labelText: 'Description'),
                ),
                TextField(
                  controller: imageController,
                  decoration: const InputDecoration(labelText: 'Path Image'),
                ),
                TextField(
                  controller: priceController,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(labelText: 'Price'),
                ),
                TextField(
                  controller: sizeController,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(labelText: 'Size'),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text("Cancel"),
              onPressed: () => Navigator.of(context).pop(),
            ),
            TextButton(onPressed: addProduct, child: const Text("Add")),
          ],
        );
      },
    );
  }

  void _showDeleteConfirmationDialog(BuildContext context, String documentId) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Confirm Delete"),
          content: const Text("Are you sure you want to delete this product?"),
          actions: <Widget>[
            TextButton(
              child: const Text("Cancel"),
              onPressed: () => Navigator.of(context).pop(),
            ),
            TextButton(
              child: const Text("Delete"),
              onPressed: () {
                deleteProduct(documentId);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void deleteProduct(String documentId) async {
    try {
      await FirebaseFirestore.instance
          .collection('products')
          .doc(documentId)
          .delete();
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Product successfully deleted')));
    } catch (e) {
      print('Error deleting product: $e');
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Failed to delete product')));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Manage Products"),
        backgroundColor: Colors.purple,
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('products').snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(
                Color(0xFF755DC1),
              ),
            ));
          }
          return ListView.builder(
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (context, index) {
              DocumentSnapshot document = snapshot.data!.docs[index];
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;

              return FadeTransition(
                opacity: Tween<double>(begin: 0.0, end: 1.0).animate(
                  CurvedAnimation(
                    parent: AnimationController(
                        vsync: this,
                        duration: const Duration(milliseconds: 500))
                      ..forward(),
                    curve: Curves.easeIn,
                  ),
                ),
                child: Card(
                  elevation: 2,
                  margin:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  child: ListTile(
                    leading: Image.asset(data['image'], width: 100, height: 80),
                    title: Text(data['title']),
                    subtitle:
                        Text('Price: \$${data['price']} Size: ${data['size']}'),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                          icon: const Icon(Icons.edit),
                          onPressed: () => _showEditProductDialog(document),
                        ),
                        IconButton(
                          icon: const Icon(Icons.delete),
                          onPressed: () => _showDeleteConfirmationDialog(
                              context, document.id),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _showAddProductDialog,
        tooltip: 'Add Product',
        child: const Icon(Icons.add),
      ),
    );
  }
}
