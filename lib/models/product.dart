import 'package:flutter/material.dart';

class Product {
  final String id; // Changed to String to handle Firestore document IDs
  final String image, title, description;
  final int price, size;
  final Color color; // This will be handled differently for Firebase

  Product({
    required this.id,
    required this.image,
    required this.title,
    required this.description,
    required this.price,
    required this.size,
    required this.color,
  });

  Map<String, dynamic> toMap() {
    return {
      'image': image,
      'title': title,
      'description': description,
      'price': price,
      'size': size,
      'color': color.value, // Store color as an integer
    };
  }

  factory Product.fromMap(Map<String, dynamic> map, String documentId) {
    return Product(
      id: documentId,
      image: map['image'] ?? '',
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      price: map['price'] ?? 0,
      size: map['size'] ?? 0,
      color: Color(map['color'] ?? 0xFFFFFFFF), // Default color is white if not specified
    );
  }
}
