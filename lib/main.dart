import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart'; 
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:mini_ecommerce/pages/cart_page.dart';
import 'package:mini_ecommerce/providers/locale_provider.dart'; 
import 'package:provider/provider.dart';
import 'package:mini_ecommerce/pages/main_page.dart';
import 'package:mini_ecommerce/pages/promote_pae.dart';
import 'package:mini_ecommerce/pages/setting_page.dart';
import 'package:mini_ecommerce/screen/login_screen.dart';
import 'package:mini_ecommerce/screen/sign_up_screen.dart';
import 'package:mini_ecommerce/screen/verify_screen.dart';
import 'package:mini_ecommerce/providers/theme_provider.dart'; // Import the ThemeProvider

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "AIzaSyDW6z-KNG3XQyWdqK8BlV8D1sAlOPtFs2o",
      projectId: "boyproject123-3bb14",
      messagingSenderId: "871787105239",
      appId: "1:871787105239:android:35de996f203113adc45992",
      storageBucket: 'boyproject123-3bb14.appspot.com',
    ),
  );
    runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => LocaleProvider()),
        ChangeNotifierProvider(create: (_) => ThemeProvider()),
      ],
      child: MyApp(),
    ),
  ); 
}

class MyAppWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ThemeProvider(),
      child: MyApp(),
    );
  }
}

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  PageController controller = PageController(initialPage: 0);

  MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    var localeProvider = Provider.of<LocaleProvider>(context);
    var themeProvider = Provider.of<ThemeProvider>(context);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Food E-Commerce',
      theme: themeProvider.themeData,
      locale: localeProvider.locale, // Use the locale from localeProvider 
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: const [
        Locale('en', ''), // English
        Locale('lo', ''), // Lao
      ],
      initialRoute: '/',
      routes: {
        '/': (context) => const PromotePage(),
        '/cart': (context) => const CartPage(),
        '/login': (context) => const LoginScreen(),
        '/register': (context) => const SignUpScrren(),
        '/verifyOtp': (context) => const VerifyScreen(),
        '/mainPage': (context) => const Main_Page(),
        '/settings': (context) => SettingsPage()
      },
    );
  }
}
