import 'package:flutter/material.dart';
import 'package:mini_ecommerce/widgets/app_search_bar.dart';
import 'package:mini_ecommerce/widgets/category_section.dart';
import 'package:mini_ecommerce/widgets/customerDrawer.dart';
import 'package:mini_ecommerce/widgets/product_grid.dart';
import 'package:mini_ecommerce/widgets/promotional_banner.dart';

class Main_Page extends StatefulWidget {
  const Main_Page({super.key});

  @override
  State<Main_Page> createState() => _Main_PageState();
}

class _Main_PageState extends State<Main_Page> {
  final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>(); // Key to interact with Scaffold

  void _toggleDrawer() {
    if (_scaffoldKey.currentState != null) {
      if (_scaffoldKey.currentState!.isDrawerOpen) {
        _scaffoldKey.currentState!.openEndDrawer();
      } else {
        _scaffoldKey.currentState!.openDrawer();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        drawer: CustomDrawer(),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                AppSearchBar(cartItemCount: 5, onMenuTap: _toggleDrawer),
                PromotionalBanner(),
                CategorySection(),
                ProductGrid(),
              ],
            ),
          ),
        ));
  }
}
