import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:mini_ecommerce/providers/theme_provider.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var themeProvider = Provider.of<ThemeProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Settings"),
      ),
      body: Center(
        child: SwitchListTile(
          title: const Text("Dark Mode"),
          value: themeProvider.themeData.brightness == Brightness.dark,
          onChanged: (bool value) {
            themeProvider
                .setTheme(value ? ThemeData.dark() : ThemeData.light());
          },
        ),
      ),
    );
  }
}
